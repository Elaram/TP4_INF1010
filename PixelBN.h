//
// Created by Gabriel Bernard
//

#ifndef TP3_PIXELBN_H
#define TP3_PIXELBN_H

#include "Pixel.h"
#include "TypePixel.h"

class PixelBN: public Pixel
{
public:
	PixelBN();
	PixelBN(bool p);
	virtual ~PixelBN();

	// Accesseur
	virtual unchar retournerR() const;
	virtual unchar retournerG() const;
	virtual unchar retournerB() const;
	bool obtenirDonnee() const;

	// Mutateur
	virtual void mettreEnNegatif();
	virtual Pixel* retournerCopieProfonde() const;
	unchar* convertirPixelCouleur() const;
	virtual unchar convertirPixelGris() const;
	virtual bool convertirPixelBN() const;

		
private:
	bool donnee_;
};

#endif // TP3_PIXELBN_H