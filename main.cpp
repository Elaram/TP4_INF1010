
#ifndef PAUSE
    #ifdef _WIN32
        #include <Windows.h>
        #define PAUSE system("PAUSE")
    #else
        #include <unistd.h>
        #define PAUSE sleep(0)
    #endif
#endif /* PAUSE */

#include <iostream>
#include <string>

#include "Pixel.h"
#include "TypePixel.h"
#include "PixelCouleur.h"
#include "PixelGris.h"
#include "PixelBN.h"
#include "TypeImage.h"
#include "Image.h"
#include "GroupeImage.h"


using namespace std;

int main() {
// Chemin vers les images
	//string cheminLoad = "./Ensemble d'images/Originale/";
	string cheminLoad = "/Users/alexa/Desktop/TP4_1688837_1748834/Ensemble d'images/Originale/";
	//string cheminSave = "/Users/alexa/Desktop/TP4_1688837_1748834/Ensemble d'images/";
    // Nom des 6 images dans un tableau de string
    string image[6] = {"Breaking-Bad.bmp", "Couleur.bmp", "Fall.bmp", "RM.bmp", "SolarEclipse.bmp", "WiC.bmp"};

    // Creer un groupe d'image nomme groupeImage1
    cout << "**************************************************" << endl;
    cout << "Creation du groupe d'image" << endl;
    cout << "**************************************************" << endl << endl;
	
	GroupeImage* groupeImage1 = new GroupeImage();

    // Ajouter les 3 premieres images contenue dans le tableau de string
    // image au groupeImage1, en couleur
	for (unsigned int i = 0; i < 3; i++) {
		Image* img = new Image(cheminLoad + image[i], TypeImage::Couleurs);
		groupeImage1->ajouterImage(img);
	}

    // Creer un deuxieme groupe image identique au groupeImage precedent
    // en utilisant le constructeur par copie ou l'operateur egal (votre choix).
    // Nomme ce groupe groupeImage2
	GroupeImage* groupeImage2 = new GroupeImage(*groupeImage1);

    // Afficher le contenu de groupeImage1
	cout << endl << "GroupeImage1: ";
	cout << *groupeImage1 << endl;

    // Afficher le contenu de groupeImage2
	cout << "GroupeImage2: ";
	cout << *groupeImage2; 

    // Ajouter toutes les images du tableau de string image au groupeImage2, en couleur
	for (unsigned int i = 0; i < sizeof(image)/sizeof(image[0]); i++) {
		Image* img = new Image(cheminLoad + image[i], TypeImage::Couleurs);
		groupeImage2->ajouterImage(img);   // Note : les images 0 � 2 ne sont pas ajout� comme elle sont d�ja pr�sentes
	}

    // Tentez de convertir toutes les images d'un des groupes en Couleur
	groupeImage2->toutMettreEnCouleur();

    // Mettre toutes les images du groupeImage1 en negatif
	groupeImage1->toutMettreEnNegatif();

    // Enregistrer toutes les images du groupeImage2
	groupeImage2->toutEnregistrer();
			
    // Convertir toutes les images du groupeImage2 en gris
	groupeImage2->toutMettreEnGris();

    // Enregistrer toutes les images du groupeImage2
	groupeImage2->toutEnregistrer();

    // Convertir toutes les images du groupeImage2 en Noir et Blanc
	groupeImage2->toutMettreEnNB();

    // Enregistrer toutes les images
	groupeImage2->toutEnregistrer();


    // N'oubliez pas de desallouer la memoire si necessaire
    // Permet d'afficher le contenu de la console
    PAUSE;

    // Fin du programme
    return 0;
}