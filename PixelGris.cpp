//
// Created by Gabriel Bernard
//

#include "PixelGris.h"

// Constructeur par d�faut
PixelGris::PixelGris() : Pixel() 
{}

// Constructeur par copie profonde
Pixel* PixelGris::retournerCopieProfonde() const {
	unsigned int i = donnee_;
	Pixel* copie = new PixelGris(i);
	return copie;
}

PixelGris::PixelGris(unsigned int donnee) : Pixel() {
    if(donnee < VALEUR_MAX_PIXEL) {
        donnee_ = donnee;
    } else {
        donnee_ = VALEUR_MAX_PIXEL;
    }
}

PixelGris::~PixelGris() {}

// Accesseur
unchar PixelGris::retournerR() const {
	return donnee_;
}
unchar PixelGris::retournerG() const {
	return donnee_;
}
unchar PixelGris::retournerB() const {
	return donnee_;
}
unsigned int PixelGris::obtenirDonnee() const {
	return donnee_;
}


// Inverser la valeur d'un pixel
void PixelGris::mettreEnNegatif() { 
	donnee_ = 255 - donnee_;
}

// Conversion entre les types de pixels (Couleur, BN, Gris)
unchar* PixelGris::convertirPixelCouleur() const {
	unchar *ret = new unchar[3];
	ret[0] = ret[1] = ret[2] = donnee_;
	return ret;
}

bool PixelGris::convertirPixelBN() const {
	return donnee_ > 127;
}

unchar PixelGris::convertirPixelGris() const {
	return donnee_;
}

