//
// Created by Gabriel Bernard
//

#include "PixelBN.h"

// Constructeur par d�faut
PixelBN::PixelBN() : Pixel() {
	donnee_ = false;
}

// Constructeur par param�tres
PixelBN::PixelBN(bool p) : Pixel() {
	donnee_ = p;
}

// Destructeur
PixelBN::~PixelBN() {}


// Accesseur
unchar PixelBN::retournerR() const {
	return donnee_*255;
}
unchar PixelBN::retournerG() const {
	return donnee_*255;
}
unchar PixelBN::retournerB() const {
	return donnee_*255;
}
bool PixelBN::obtenirDonnee() const {
	return donnee_;
}


// Mutateur
void PixelBN::mettreEnNegatif() { 
	donnee_ = 255 - donnee_;
}

// Fonction de copie profonde d'un pixel
Pixel* PixelBN::retournerCopieProfonde() const {
	Pixel* copie = new PixelBN(donnee_);
	return copie;
}

// Conversion des pixels entre les types (Couleur, BN et Gris)
unchar* PixelBN::convertirPixelCouleur() const {
    unchar valeur = (unchar)VALEUR_MIN_PIXEL;
    if(donnee_) {
        valeur = (unchar)VALEUR_MAX_PIXEL;
    }

	unchar* ret = new unchar[3];
	ret[0] = ret[1] = ret[2] = valeur;

	return ret;
}
bool PixelBN::convertirPixelBN() const {
	return donnee_;
}

unchar PixelBN::convertirPixelGris() const {
	return donnee_ ? VALEUR_MAX_PIXEL : VALEUR_MIN_PIXEL;
}