//
// Created by Gabriel Bernard
//

#include <iostream>
#include "GroupeImage.h"


using namespace std;

// Constructeur par d�faut
GroupeImage::GroupeImage() {};

// Constructeur par copie
GroupeImage::GroupeImage(const GroupeImage& grImg) 
{
	for (unsigned int i = 0; i < grImg.images_.size(); i++){
		images_.push_back(new Image(*grImg.images_[i]));
	}
}

// Destructeur
GroupeImage::~GroupeImage() {};


// Ajouter une image au groupe d'images 
bool GroupeImage::ajouterImage(Image* image) {
    for (unsigned int i = 0; i <  images_.size(); i++)
    {
        if (image->obtenirNomImage() == *(images_[i]))
        {
            cout << image->obtenirNomImage() << " n'a pas ete ajoute" << std::endl;
            return false;
        }
    }
    images_.push_back(image);
    std::cout << image->obtenirNomImage() << " a bien ete ajoute" << std::endl;
    return true;
}

// Retirer une image au groupe d'images
bool GroupeImage::retirerImage(const std::string& nom) {
    for (unsigned int i = 0; i < images_.size(); i++)
    {
        if (*(images_[i]) == nom)
        {
            images_[i] = images_.back();
            images_.pop_back();
            cout << nom << " a bien ete retire" << endl;
            return true;
        }
    }
    return false;
}

// Fonction d'affichage de toutes les images contenues dans un groupe d'images
void GroupeImage::afficherImages(ostream& os ) const {

    os << endl;
    os << "**************************************************" << endl;
    os << "Affichage des images du groupe :  " << endl;
    os << "**************************************************" << endl << endl;

    for  (unsigned int j= 0; j < images_.size(); j++) {
        os << *images_[j] << "--------------------------------------------------" << endl;
    }
    os << endl;
}

// Accesseur
Image* GroupeImage::obtenirImage(unsigned int indiceImage) const {
    return images_[indiceImage];
}


// Fonction de surcharge
GroupeImage& GroupeImage::operator+=(Image* image)
{
    ajouterImage(image);
    return *this;
}

// Surcharge des op�rateurs
GroupeImage& GroupeImage::operator-=(Image* image)
{
    retirerImage(image->obtenirNomImage());
    return *this;
}

std::ostream& operator<<(std::ostream& os, const GroupeImage& groupeImage)
{
    os << endl;
    os << "**************************************************" << endl;
    os << "Affichage des images du groupe :  " << endl;
    os << "**************************************************" << endl << endl;

    for  (unsigned int j= 0; j < groupeImage.images_.size(); j++) {
        os << *groupeImage.images_[j] << "--------------------------------------------------" << endl;
    }
    os << endl;

    return os;
}

Image* GroupeImage::operator[](const unsigned int& indice) const {
    if(indice >= images_.size()) {
        cerr << "L'indice est plus grand que la quantite d'image present dans ce groupe." << endl;
        return nullptr;
    }
    return images_[indice];
}


//Overload de l'operateur =
GroupeImage& GroupeImage::operator=(const GroupeImage& grImg)
{
	if (this != &grImg) {
		// Enlever les images pr�sente dans le groupe vers lequel on veut copier
		for (unsigned int i = 0; i < images_.size(); i++) {
			delete images_[i];
		}
	}
	// Ajouter les images au groupe images vers lequel on veut copier
	for (unsigned int i = 0; i < images_.size(); i++){
		images_.push_back(new Image(*grImg.images_[i]));
	}

	return *this;
}


// Accesseur
unsigned int GroupeImage::obtenirNombreImages() const {
    return images_.size();
}


// Fonctions de modification des images contenues dans un groupe
	// Modification du type de la couleur des images
void GroupeImage::toutMettreEnNB(){
    unsigned int i;
    for(i = 0; i < images_.size(); i++) {
        images_[i]->convertirNB();
    }
    cout << endl;
}
void GroupeImage::toutMettreEnGris() {
    unsigned int i;
    for(i = 0; i < images_.size(); i++) {
        images_[i]->convertirGris();
    }
    cout << endl;
}

void GroupeImage::toutMettreEnCouleur() {
    unsigned int i;
    for(i = 0; i < images_.size(); i++) {
        images_[i]->convertirCouleurs();
    }
    cout << endl;
}
	// Inversion de la couleur des images
void GroupeImage::toutMettreEnNegatif() {
	unsigned int i;
	for (i = 0; i < images_.size(); i++) {
		images_[i]->toutMettreEnNegatif();
	}
	cout << endl;
}

// Enregistrement de l'image 
void GroupeImage::toutEnregistrer() {
    unsigned int i;
    for(i = 0; i < images_.size(); i++) {
        Image* image = images_[i];
		string nom = "/Users/alexa/Desktop/TP4_1688837_1748834/Ensemble d'images/" + image->obtenirTypeEnString()
			+ "/im" + to_string(i) + image->obtenirNomImage();

        cout << "Sauvegarde de " << nom << endl;
        images_[i]->sauvegarderImage(nom);
    }
    cout << endl;
}