//
// Created by Gabriel Bernard
//

#ifndef TP3_PIXEL_GRIS_H
#define TP3_PIXEL_GRIS_H

#include "Pixel.h"

class PixelGris :
	public Pixel
{
public:
	PixelGris();
	PixelGris(unsigned int);
	virtual ~PixelGris();
	// Accesseur 
	virtual unchar retournerR() const;
	virtual unchar retournerG() const;
	virtual unchar retournerB() const;
	unsigned int obtenirDonnee() const;

	// Mutateur
	virtual void mettreEnNegatif();
	virtual Pixel* retournerCopieProfonde() const;
	virtual bool convertirPixelBN() const;
	unchar* convertirPixelCouleur() const;
	virtual unchar convertirPixelGris() const;
	

private:
	unchar donnee_;
};

#endif // TP3_PIXEL_GRIS_H