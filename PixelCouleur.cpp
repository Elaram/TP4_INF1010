//
// Created by Gabriel Bernard
//

#include "PixelCouleur.h"


PixelCouleur::PixelCouleur() : Pixel() {
	donnee_[Couleur::R] = donnee_[Couleur::G] = donnee_[Couleur::B] = VALEUR_MIN_PIXEL;
}

// Constructeur par paramètres
PixelCouleur::PixelCouleur(const unchar& r, const unchar& g, const unchar& b) : Pixel() {
    donnee_[Couleur::R] = testPixel(r);
    donnee_[Couleur::G] = testPixel(g);
    donnee_[Couleur::B] = testPixel(b);
}


// Destructeur
PixelCouleur::~PixelCouleur() {}

// Accesseur
unchar PixelCouleur::retournerR() const {
	return donnee_[Couleur::R];
}

// Accesseur
unchar PixelCouleur::retournerG() const {
    return donnee_[Couleur::G];
}

unchar PixelCouleur::retournerB() const {
    return donnee_[Couleur::B];
}

unsigned int PixelCouleur::somme() const {
	unsigned int somme = donnee_[Couleur::R] + donnee_[Couleur::G] + donnee_[Couleur::B];
	return somme;
}

// Inversion de la couleur des pixels
void PixelCouleur::mettreEnNegatif() {
	donnee_[Couleur::R] = 255 - donnee_[Couleur::R];
	donnee_[Couleur::G] = 255 - donnee_[Couleur::G];
	donnee_[Couleur::B] = 255 - donnee_[Couleur::B];
}

// Retourner la copie profonde d'un pixel
Pixel* PixelCouleur::retournerCopieProfonde() const { 
	Pixel* copie = new PixelCouleur(donnee_[Couleur::R], donnee_[Couleur::G], donnee_[Couleur::B]);
	return copie;
}

// Conversion entre les types de pixels (De Couleur vers BN ou Gris)
bool PixelCouleur::convertirPixelBN() const {
	unsigned int moyenne = somme() / 3;
	return moyenne > 127;
}

unchar PixelCouleur::convertirPixelGris() const {
	return somme() / 3;
}

// Fonctions de modification
void PixelCouleur::modifierTeinteRouge(unchar d) {
	donnee_[Couleur::R] = d;
}
void PixelCouleur::modifierTeinteVert(unchar d) {
	donnee_[Couleur::G] = d;
}
void PixelCouleur::modifierTeinteBleue(unchar d) {
	donnee_[Couleur::B] = d;
}